# kinesis-aggregation-ts

`aws-kinesis-agg` の型定義が壊れてたのでいい感じにラップしました。

## usage

```ts
import { deaggregate } from 'kinesis-aggregation-ts'

import * as zlib from 'zlib'
import { promisify } from 'util'
const unzip = promisify(zlib.unzip)

await Promise.all(event.Records.map(r => deaggregate(r.kinesis, true)))
  .then(list => list.flat(1))
  .then(async list => {
    const newlist = await Promise.all(
      list.map(async item => {
        // aws-fluent-plugin-kinesis で aggregate 前に gzip かけてるなら 要 unzip
        const data = await unzip(item.data).then(
          b => Buffer.isBuffer(b) && b.toString(),
        )
        return { item, data }
      }),
    )
  })
```
