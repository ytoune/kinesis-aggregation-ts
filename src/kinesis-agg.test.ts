import { aggregate, deaggregate } from './kinesis-agg'

const flat = <T>(ls: (T[] | undefined)[]) => {
	const r: T[] = []
	ls.map(l => l?.map(i => r.push(i)))
	return r
}

test('aggregating and deaggregating should not fail', async () => {
	const start = Array.from({ length: 9 }, (_, i) => i + 1).map(id => ({
		partitionKey: 'piyo',
		data: 'data' + id,
	}))
	const encoded = await aggregate(start)
	expect(Buffer.isBuffer(encoded[0].data)).toBe(true)
	const parsed = await Promise.all(
		encoded.map((r, num) => {
			const sequenceNumber = `${num}`
			const data = r.data.toString('base64')
			return deaggregate({ sequenceNumber, ...r, data }, true)
		}),
	).then(flat)
	expect(parsed[8].data.toString()).toBe('data9')
})
