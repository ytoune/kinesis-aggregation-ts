export {
	RecordPayload,
	UserRecord,
	EncodedRecord,
	aggregate,
	deaggregate,
} from './kinesis-agg'
